# S3 Buckets

The DMA enables provisioning of S3 Buckets on the EMBL Minio Server. Refer to the [intranet page](https://www.embl.org/internal-information/it-services/s3-object-store/) for detailed information.

## Billing
During the bucket provision process you will be asked to provide a budget number which will be charged for the usage of the bucket. Information on pricing can be found on the [intranet page](https://www.embl.org/internal-information/it-services/s3-object-store/)

## Bucket Naming Rules
When creating a bucket via the DMA you must specify a unique name which meets all of the following criteria: 

* Bucket names must be between 3 (min) and 63 (max) characters long.
* Bucket names can consist only of lowercase letters, numbers, dots (.), and hyphens (-).
* Bucket names must not contain two adjacent periods, or a period adjacent to a hyphen.
* Bucket names must not be formatted as an IP address (for example, 192.168.5.4).
* Bucket names must not start with the prefix xn--.
* Bucket names must not end with the suffix -s3alias. This suffix is reserved for access point alias names.

The name must be unique and can not be used by another EMBL user on the Minio system.

If the name specified does not meet all of the above, you will see an error message when you try to create the bucket. 

## Bucket Deletion
Buckets which are created using the DMA can only be deleted using the DMA. A bucket can only be deleted from Minio if it is empty.

You can delete a bucket via the UI or API by requesting a `Delete` action for the Bucket.

## Bucket Quotas
The DMA also allows you to set an optional quota for your bucket. This can be done during creation, but can also be set / adjusted later via the DMA. A bucket quota is a maximum size for your bucket. Once the size of the data in the bucket reaches the quota you will not be able to add any more data unless you increase the quota or remove some data.

## Sharing Buckets
The DMA facilitates sharing buckets with other EMBL users, similar to sharing regular data items via the DMA UI and API. You simply need to select the user you wish to share with and define their permission level (read-only/read-write).

Note: Bucket sharing via the DMA only allows sharing with EMBL Heidelberg users with an active LDAP account.

Once shared, the user can access the bucket via the Minio console which they can log into using their EMBL credentials. 

### Technical details of sharing with EMBL users
Sharing buckets via the DMA uses Minio policies. When a bucket is created via the DMA, three predefined policies are generated on the Minio server which reference the bucket. Sharing a bucket via the DMA assignse one of these policies to the user you are sharing with.

For instance, creating a bucket named `test_bucket` adds these three canned policies on the Minio server:

- `test_bucket_admin`: Grants full S3 permissions on the bucket - only applied to the user who provisioned the bucket.
- `test_bucket_ro`: Allows listing and reading the bucket's content.
- `test_bucket_rw`: Enables reading from and uploading to the bucket.

Upon Minio UI login, the user's policy list is fetched from the server, allowing them to view accessible buckets. Additionally, users can generate access tokens mirroring their permissions or a subset thereof.

### Making Buckets Public
You can use the DMA to make a bucket publicly readable. This can be done via the UI or the API by sending a share action request with the userId set to *.

When you make a bucket public, all of its contents become accessible without authentication. Users can access the data using any S3 client or by visiting s3.embl.de/{BUCKET_NAME} in their web browser.

Making a bucket public will overwrite its existing bucket policy, replacing it with the following:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:GetObject",
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::foo-bar-1/*",
                "arn:aws:s3:::foo-bar-1"
            ]
        }
    ]
}
```

### Making Buckets Private
You can also use the DMA to make a bucket private. This can be done via the UI or the API by sending an unshare action request with the userId set to *.

Doing so will replace the bucket's policy with the following:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:*"
            ],
            "Resource": [
                "arn:aws:s3:::foo-bar-1/*"
            ]
        }
    ]
}
```

Note: Any internal EMBL users with whom you have shared the bucket via the DMA will retain access. Additionally, all existing access keys will remain valid and can still be used to access the data.