# DMA Introduction

## Logging in

You can access the application using your EMBL username and password.

## Basic Terms

### Data

Data is a representation of a file or folder location on disk. It can also be an S3 bucket. It has the following attributes:

- A name
- A path to file/folder on disk or a URL for an S3 bucket
- A collection to which it is part of
- Optional metadata such as attached JSON or a simple comment.

### Collections

A collection is a group of Data. It can be used to organize your data. Collections allow you to easily apply actions to all data that is part of a collection, e.g., Archival.

### Actions

An action is applied to a collection or data and includes file system operations such as Archive, Restore, Delete, and Share. More information about each of these actions can be found in the action section below.

## Data

### Register Data

To register Data, click the "Register Data" button on the top left of the 'Your Data' page.

This will open up a file browser where you can see the data on your group share. If you are a member of more than 1 group, you will see a drop-down menu allowing you to switch between different group shares.

Simply browse to the data you wish to register using the file browser and choose the file/directory you want.

After selection, you will see the 'Name' field value update to the file/directory name. You can change this value if you wish.

You will also be asked to select the collection to which the new data should be added. You can choose an existing collection using the drop-down menu or select the 'Create New Collection' option if you wish to create a new collection. If you choose this option, you will then see 2 new text fields appear allowing you to add a collection name and description.

### Your Data

The 'Your Data' page allows you to get an overview of all of your Data in the DMA.

The icons on the right side of the grid allow you to get a quick overview of the data status and the actions that have been requested for each data. You can also sort using these columns to quickly find data with a certain status.

To get more information about a data item, simply click on the data in the list. This will open the data overview panel. From here, you can see an overview of the data, the data content on the file system, and request actions for the data.

## Collections

Collections allow you to organize and group your data in a way that works for you. Every data item in the DMA must be registered as part of a collection. Actions such as Archival can also be performed on the collection level.

### Create a new collection

To create a new Collection, simply click the "Add Collection" button on the top left corner of the 'Your Collections' page. Supply a meaningful name and description and click "Create Collection".

Collections can also be created during the creation of registration data.

The 'Your Collections' page provides an overview of all your Collections.

### Your Collections

The 'Your Collections' page allows you to get an overview of all of your Collections in the DMA.

You can search collections, filter and sort them by Name and Created columns.

To get more information about a Collection item, simply click on the Collection in the list. This will open the Collection overview panel. From here, you can see an overview of the Collection and which Data is inside it.
