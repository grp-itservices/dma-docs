# DMA Training

This training session aims to introduce users to the basic concepts of the Data Management Application (DMA), as well as teach users how to use common and advanced features of the DMA. 

???+ tip  "Download Slides"

    You can download a copy of the presentation which was given on October 26, 2023 [here](https://git.embl.de/grp-itservices/dma-docs/-/raw/main/docs/training/slides/dma_training_slides_26102023.pdf).

???+ info  "Use DMA Training Environment"

    To follow this training guide please use the [DMA Training Environment](#dma-training-environment).

## DMA Training Environment
The DMA Training Environment serves as a dedicated platform to acquaint you with the features of DMA without the requirements of registration or impacting your personal data.

- **Access**: Visit the [DMA Training Environment](https://dma-training.embl.de).

- **Login Information**: Use any of the [provided user credentials](https://docs.google.com/spreadsheets/d/185UpPaqqqIzFiqI3CT4n06g_F-C61zKzZJFYwAnYNpI/edit?usp=sharing). All users utilize the same password in this environment: **acciodata**.

- **Simulation Only**: Activities within this environment are entirely simulated. No genuine data alterations will be executed. Furthermore, there are no incurred charges for operations, such as archival. Operations will automatically be marked as `Done` after a random 30-60 seconds wait time.

- **Email notifications**: All email alerts will be available in the [shared mailbox](https://dma-training.embl.de/mail/). Search for your email address (username@hogwarts.org) to find relevant mails.

- **API Access**: The training environment also offers full access via API.

- **Weekly Reset**: The training environment undergoes a reset every Monday night. As a result, any progress or actions from the prior week will be lost.

- **Limitations**: Some features are not available in the training environment, for example S3 Bucket Creation.

You are very welcome to use this environment at any time to play around with and get to know the system.

## Hands On 1: Training Environment Login
???+ abstract  "Hands On 1: Login to the DMA Training Environment"

    We will now login to the Training Environment. 
    
    1. Claim your training environment user [here](https://docs.google.com/spreadsheets/d/185UpPaqqqIzFiqI3CT4n06g_F-C61zKzZJFYwAnYNpI/edit?usp=sharing) by putting your EMBL username in the empty column beside an available user.

    2. Once you have selected a username, you can log into the [DMA Training Environment](https://dma-training.embl.de) with your selected username and the password *acciodata*.

    Once you are logged in you should see an empty `Your Data` view.

## DMA User Interface (UI) Training
### Data Registration
To make the DMA aware of your data you need to first register the data. The DMA UI provides a file browser to allow you to easily browse and register your data from your group share(s). 

You will only be able to see data which you can see on your group share - so if you do not have permissions to view the files on the file system they will also not be displayed here. 

It is important to note registering the data simply makes the DMA aware of the existence of your data. We do not modify the data on the file system in any way after registration. You still have full control of your data on the file system. This means you are responsible for keeping the DMA and your files on the file system in sync. If you manually move data, delete data, change permissions of data etc. on the file system, the DMA will not be aware of these changes and they will not be tracked.

On registration, your data will be assigned a DMA ID. This ID is unique and can be used later to refer to your data within the DMA. Each data item is accessible via URL using the data ID: `https://dma-training.embl.de/data?id=<YOUR_DATA_ID>` allowing you to easily reference your DMA data in other places. 

#### Data Organisation
The DMA also provides you with the ability to organise your data in a way which makes sense for you using collections and projects. 

##### Collections
A collection is a group of data. It can be used to organize your data. Collections allow you to easily apply actions to all data that is part of a collection, e.g., Archival. 

It is important to note that every data item must be registered as part of a collection.

A collection can only contain data from a single volume / group share. 

Each collection is also accessible via URL using the collection ID: `https://dma-training.embl.de/collections?id=<YOUR_COLLECTION_ID>` .

##### Projects
A project is a group of collections. It is solely used for organisational purposes. The DMA UI currently does not support projects, and they are only accessible via the API. If you have a special use case which would require the use of projects, reach out and we can discuss. 

#### Registration Restrictions
There are a few rules when it comes to data registration in the DMA:

    1. All data must be assigned to a collection.
    2. You can not register data which is a child of an already registered data.
    3. You can not register data which is a parent of an already registered data. 

#### Hands On 2: Register Data
???+ abstract  "Hands On 2: Register Your Charms Class Data"
    We will now register your first Data item in the DMA. As mentioned each data item must be registered inside a collection, so we will first create a collection. 

    **Create Collection:**

    1. Click the `Your Collections` menu item on the left hand menu.
    2. Click the `Add Collection` button.
    3. Create a new collection with the name `Schoolwork 2023` and with the following description: `My Hogwarts School work for 2023`.
    4. Click `Create Collection`.

    **Register Data:**

    Now that we have our first collection we can go ahead and register our first data. We will register our schoolwork for the `Charms` class. 

    1. Click the `Your Data` menu item on the left menu. 
    2. Click the `Register Data` button.
    3. Browse to your `charms` directory - `/<GROUP>/<YOUR_USERNAME>/charms`. Select the `charms` directory.
    4. Enter `charms_work` as the `Data Name`.
    5. Enter `My charms class work` as the `Comment`.
    6. Select the `Schoolwork 2023` collection from the `Collection` drop down menu. 
    7. Click `Register Data`

    Once data is registered, you are free to rename it or add / edit the data comment at any time. To do this, open the data overview and click the `Edit Data Info` button. Click the `Save changes` button once you are happy with you changes.

    Go ahead and update the comment of your data to `My charms class work for 2023`.


#### Deregistering Data
It is possible to Deregister Data from the DMA if the data is still in a `clean` state, i.e. no actions have been performed, no metadata attached etc. This allows you to undo any registration which may have been a mistake. If the data is no longer in a `clean` state then the `Deregister` button will not be visible.

#### Hands On 3: Deregistering Data
???+ abstract  "Hands On 3: Deregister Data"
    **Deregister Data:**

    You suddenly realise that this was not actually the data you wanted to register, instead you wanted to register each file inside the charms directory seperately, so you should now deregister this data. 

    1. Open the data overview panel for the `charms_work` data by selecting it from the `Your Data` view. 
    2. Click the `Deregister` button.
    3. Click the `Deregister Data` button in the pop up.


#### Child Data
We do allow registering child data but this means the parent can not be acted on anymore. This feature is only supported via the API for now and should only be used in exceptional use cases. 

### Actions

An Action is an operation applied to data. Certain actions can also be applied to collections. The DMA supports 4 different types of actions in the UI:
    
    1. Archive
    2. Restore
    3. Share
    4. Delete

The API also supports 3 more advanced actions: Handover, Rename and Extract.

The list of available actions will continue to grow as new features are added to the DMA, but the core idea will remain the same for each action type.

Actions are asyncronous, and take some time to be completed. Once an action is requested it will be in a `pending` state. Once it has actually completed in the backend, it will be updated to reflect the status to either `done` or `failed`. The status of an action is always visible via the DMA UI and when the status of an action changes the user will be notified by email.

### Share

One of the actions you can apply to data is sharing. This is useful when you want to share your data with a single collaborator who is **not** in your EMBL group (e.g. from another unit/research group).

#### Hands On 4: Share Data
???+ abstract  "Hands On 4: Share Data"
    Let's imagine that Professor Severus Snape gave you Defence Against the Dark Arts homework to do and you produced your results in a file on your group share. Now you want to give Professor Snape access to read your homework, otherwise he might expel you from Hogwarts.

    First of all, you need to register your homework file as a new data.

    **Register Data:**

    1. Click the `Your Data` menu item on the left menu. 
    2. Click the `Register Data` button.
    3. Browse to your `Defence Against the Dark Arts` directory - `/<GROUP>/<YOUR_USERNAME>/defense_against_the_dark_arts`. Select the `homework.txt` file.
    4. Enter `defense_against_the_dark_arts_homework` in the `Data Name` field.
    5. Enter `My Defence Against the Dark Arts homework` in the comment field.
    6. Select the `Schoolwork 2023` option from the `Collection` drop down menu. 
    7. Click `Register Data`

    Now let's share our homework with Professor Severus Snape. 

    **Share Data:**

    1. Click the `Your Data` menu item on the left menu. 
    2. Select the `defense_against_the_dark_arts_homework` data you just registered.
    3. Click on the `Actions` tab in the data overview panel.
    4. Open the `Share` action panel.
    5. Start typing `Severus Snape` in the `EMBL Username` dropdown and select the user.
    6. Select `Read Only` in the `Select Access Type` dropdown.
    7. Enter `Homework for Defence Against the Dark Arts` as the `Comment`.
    8. Click `Share`
        
    You have now shared your data.
    
    **Check Action Status: Pending**

    1. Select your `defense_against_the_dark_arts_homework` data and go to `Actions` -> `Requested Actions`.
    2. Select the `SHARE` action and take a look at the `Pending` status it is in.
        
    Now you want to know whether the sharing was actually successful or not.

    **Check Action Status: Done**

    1. Go to the [shared mailbox](https://dma-training.embl.de/mail) and enter your hogwarts email address in the search field - <YOUR_USERNAME>@hogwarts.org
    2. Wait for an email with the subject `Your Data Share Request has been Completed` - this should arrive 30-60 seconds after you requested the share action.
    3. Click on the email and look for the Comment which mentions `Homework for Defence Against the Dark Arts`
    4. Professor Snape will have also received an email with the subject `<YOUR_USERNAME> has Shared Data with you`.
    5. Refresh the [DMA](https://dma-training.embl.de)
    6. Select your `defense_against_the_dark_arts_homework` data and observe the newly added icon in the `Shared` column.
    7. Go to `Actions` -> `Requested Actions` again and observe that your action's status has changed to `Done`

**Shared Data**

Whenever someone shares data with you, you will get notified by email about the sharing, but you can also open the `Shared Data` menu item to get an overview of all the data that was shared with you by other users. This is a read only view, where you can see a list of all data, and get the location of the data. When data is shared with you via the DMA, you will not be able to perform actions on that data via the DMA.

### Archive
#### Archiving Data
The DMA is the only way to archive your data to the Heidelberg tape library. Once archived, your data will remain on tape for 10 years and can be restored at any time.

To archive data you will need to supply a valid Budget Number. Information about archive costs can be found [here](https://www.embl.org/internal-information/it-services/central-data-storage-file-systems-heidelberg/)

The DMA supports archiving of data of any size, however if your data consists of a very large number of files (100k +) then we do recommend that you first create a tar of your data and then submit the tar for archive. If you have any questions or need some help with this please contact us at dma@embl.de 

You also have the option to Delete the data as part of the archive process. The DMA will then take care to delete each file once it has been successfully written to the tape archive. It is important to note that the DMA will only delete files, directories will remain in place and can be cleaned up manually later if needed.

Archive requests take time, the amount of time depends on several factors including the size of your data and how busy the archive infrastructure is at any given moment. It is very normal for an archive request to take several days to complete. 

???+ warning  "Important Note"
    Once you have requested the archive of a file or directory it is important that you **DO NOT modify** this data on the file system until the requested process has been marked as complete. Modifying the data during the archive process may lead to the process failing and data loss. 

#### Hands On 5: Archive Data
???+ abstract  "Hands On 5: Archive Data"
    You have successfully passed the Herbology exam and no longer need the notes for the class, however they may be useful when you take the advanced Herbology class next year. 

    As your House Prefect insists that you keep the amount of data which is stored on your House share to minimum, you decide to archive the Herbology notes, which will allow you to restore them later if needed, while also keeping the house prefect happy.

    **Register Herbology Data**

    1. Click the `Your Data` menu item on the left menu. 
    2. Click the `Register Data` button.
    3. Browse to your `herbology` directory - `/<GROUP>/<YOUR_USERNAME>/herbology`. Select the `herbology` directory.
    4. Enter `herbology_notes` in the `Data Name` field.
    5. Enter `My herbology stuff` in the `Comment` field.
    6. Select the `Create New Collection` option from the `Collection` drop down menu. This allows us to create a new collection as part of the data registration process. 
    7. Enter `My Old Notes` in the `Collection Name`.
    8. Enter `Old stuff including Herbology` in the `Collection Description`.
    9. Click `Register Data`

    **Archive the Herbology Data**
        
    1. Open the Data overview for the `herbology_notes` data.
    2. Click the `Actions` tab on the top of the data overview.
    3. Open the `Archive & Restore` action panel.
    4. Enter `12345` as the budget number.
    5. Enter a relevant comment which can be used for yourself later to identify the archive - e.g. `Herbology notes from 2023`.
    6. Select the `Delete Files After Archive` option to make sure the data gets deleted once it is archived.
    7. Submit the archive request.

    After 30-60 seconds your Action should be marked as `Done` and you will receive an email notification [here](https://dma-training.embl.de/mail/)

    When you now open the `Archive & Restore` action panel for your `herbology_notes` data, you will see your completed Archive listed in the `Existing Archives` section. 

    You will also notice that the `Share` and `Delete` Action panels are no longer available, this is because that data was deleted as part of the archive request.

#### Archiving Collections
You can also request to archive a DMA Collection, doing so will archive each data item from that collection. A separate archive request will be created for each data item, and you can track the progress of the archive in the `Your Actions` view. Once the archive requests for each data item in the collection are complete, you will be notified by email and the collection archival request will be set to `done`. 

#### Hands On 6: Archive Collection
???+ abstract  "Hands On 6: Archive Collection"
    You notice that you have a few more items on the house share which you do not use and you could archive them.

    **Create Collection**

    We will create a new collection called `Data to archive`.

    1. Open the `Your Collections` view and choose `Add Collection`.
    2. Enter `Data to archive` as the `Collection Name` and `Data I no longer use` as the `Collection Description`.
    3. Click `Create Collection`.

    **Register Data**

    Register the following 3 files in the DMA and add them to the `Data to archive` collection:

        - `/<GROUP>/<YOUR_USERNAME>/other/jk_rowling_tweets.txt`
        - `/<GROUP>/<YOUR_USERNAME>/other/marauders_map.zip`
        - `/<GROUP>/<YOUR_USERNAME>/other/quidditch_rules.pdf`

    **Archive Collection**

    Once you have added the 3 data items to the collection return to the `Your Collections` view and select the `Data to archive` collection. We will now submit an archive request for the collection.

    1. Open the `Actions` tab on the collection overview panel.
    2. Open the `Archive` action panel.
    3. Enter `12345` as the budget number.
    4. Enter `Archive of old stuff` in the Comment section.
    5. Do **NOT** select the Delete option for now, as we first want to double check with our house prefect if the data is needed anymore. 
    6. Submit the Archive request.

    If you now navigate to the `Your Actions` page you will see 4 Archive actions in the `Pending state` - 3 actions for the data items and 1 action for the collection. 

    These actions should be marked as done after 30-60 seconds, and you will receive an email notification in the [shared mailbox](https://dma-training.embl.de/mail/).

#### Legacy Archives
All data which was archived pre-DMA is also accessible via the DMA UI. The owner of this data is set to the group leader by default, but it can also be accessed by volumes admins.

To see this data you need to enable the `Show Data from Hidden Collections` option in the Volume Data view. 

Legacy archives do not support the concept of partial restore, and archive indexs are not available for legacy archives.

### Restore
You can restore any data which you have archived to tape using the DMA. Simply choose the archive you wish to restore and provide us with a path to where the data should be restored. 

It is important that you make sure you have enough free space available for the data to be restored to the given location and that you have write permissions at the given location. Before restoring, the DMA will inform you of how much space is needed.

Just like archive processes, restore processes also take time depending on the amount of data to be restored and how busy the archive infrastructure is. 

#### Hands on 7: Restore Data
???+ abstract  "Hands On 7: Restoring Data"
    A classmate approaches you for some help preparing for their herbology exam, which they must resit as they failed the previous one. They ask for a copy of your herbology notes and homework, which you have already archived and deleted using the DMA. 

    We will now restore this data so we can help out your classmate. 

    1. Open the `Your Data` view and search for your `herbology_notes` data which you should see marked as Archived and Delete.
    2. Select the `herbology_notes` data and open the `Actions` tab in the data overview.
    3. Expand the `Archive & Restore` panel where you should see a list of `Existing Archives`
    4. Click the `Restore` button.
    5. Enter the following path in `Restore To` field - `/tmp/hogwarts/[YOUR_HOUSE]/[YOUR_TRAINING_USERNAME]`
    6. Click `Restore Data`.

    You can now open the `Requested Actions` panel, where you should see a Restore action in the `Pending` state. 

    This restore action will be updated to `Done` in 30-60 seconds and you will be informed by [email](https://dma-training.embl.de/mail/) that your restore request has been complete.

    The next step would be to register the restored data in the DMA, and then share it with your classmate, but we will skip this step for now.


#### Partial Restore
The DMA also supports partial restore of archives, allowing you to restore a single file or directory from a larger archive. The partial restore works in the same way as the full restore, but you will also specify the subdirectory or file from the archive which you want to restore. 

You will need to provide the **full path** of the file or directory which you want to restore. 

You can view the full list of files which were archived as part of an archive by downloading the archive index via the DMA UI. You can find this index in the `Existing Archives` list, in the `Archive & Restore` action panel for each data item which has been archived. Please note these lists are not available on the DMA Training Environment. 
 
### Delete
We have already seen how you can delete data as part of an Archive request, but the DMA also has a separate delete option which can be used to delete data from your group share. 

When you request data deletion a Delete action will be created and will be marked as `Done` when the request is complete. This can take some time and you will be notified by email once it is complete. 

You must have the required permissions on the file system to be able to request the deletion of data. If you do not have sufficient permissions the action will fail and you will be notified by email. 

If you happen to have permission to delete some of the data but not all the data, then the action will also fail, however some of the data may get deleted. 

#### Hands On 8: Delete Data
???+ abstract  "Hands On 8: Delete Data"
    Earlier while archiving a collection we decided to not select the `Delete Files After Archive` option because we wanted to check with our House Prefect if it was ok to delete the data. 

    The prefect has now confirmed that we can delete the `jk_rowling_tweets.txt` data but we should not delete the `marauders_map.zip` or `quidditch_rules.pdf` data just yet as it may be useful for some other students.

    We will now submit a data delete request for the `jk_rowling_tweets.txt` data.

    1. Navigate to the `Your Data` view and find the `jk_rowling_tweets.txt` data.
    2. Open the `Actions` tab in the data overview.
    3. Expand the `Delete` action panel. Note that the DMA is aware that this data has already been archived, and mentions here that you will have the option to restore if needed after deletion.
    4. Click the `Delete Data` button.
    5. You will now be prompted to confirm that you understand the consequences of data deletion. Put a check in the checkbox and click `Delete Data`

    A data `Delete` action will now be created, and will be marked as done after 30-60 seconds. You can see the `DELETE` action in the `Requested Actions` panel or in the `My Actions` page. 

    Once the actions is marked as done you will receive an email notification to the [shared mailbox](https://dma-training.embl.de/mail/)


### Advanced Actions
The DMA also features some actions which are not available to all users, and are only available via the API for now. 

In this training we will briefly discuss each of these actions and explain how they may be used. If you believe you may have a use case for any of these actions, feel free to reach out and we will discuss your use case.

#### Handover

The Handover action is useful if you want to "give" data to some other user. What actually happens in the background is that your data is copied from the source location you specify to a new target location on the file system and the ownership of the newly copied data is transferred to the user you specify.
It is also possible to copy only a subdirectory of your data to the target location, this is achieved by providing the optional parameter `limitTo`.

#### Extract

The Extract action can be used if you want to move a subdirectory of your data to another location you specify. In the background the subdirectory is moved to the new target location and a new data gets created there. There is no ownership change happening.

#### Rename

The Rename action is essentially a move. You can rename your data by specifying a new name and comment. What happens on the file system is that your data is moved to the new path.

### Group Data
The DMA allows you to view all DMA data items registered by other users in your group using the `Group Data` view. This view only allows you to see basic information about data including the data owner, path and a quick overview of the data status (archived, shared, restored).

Another useful feature available on the `Group Data` view is the `Volume Info` feature which gives you a quick overview of the volume usage for your volume. It shows the current amount of data on the share, free space and also shows a graph of this information for the past 30 days.

#### Hands On 9: Volume Info
???+ abstract  "Hands On 9: Volume Info"
    Your house prefect has asked you to check which group member has generated the most data over the past 30 days and asked you to work with them to archive some of their data to free up some space on the volume. 

    1. Navigate to the `Group Data` view using the left side menu.
    2. Click the `Volume Info` button.
    3. Order the user list based on `Last 30 days` column - this will show you which user has generated the most data recently.
    4. Click the user who has generated the most data. This will show you a graph of data usage over time for that user.

???+ note "Note"
    The volume info feature is not available for all group shares by default. If it is not available for your group please contact dma@embl.de to check if it can be enabled. 

### Finding your data
Each view of the DMA includes some form of search and filter to allow you to quickly find what you are looking for. 

The `Your Data` view allows you to filter by collection as well as search based on several data attributes such as data id, name, path and comment content. You can also filter based on date registered and sort your data based on name and registration date. 

By default, deleted data will also be listed, but you can change this using the filter menu.

#### Hands On 10: Search & Filter
???+ abstract  "Hands On 10: Search"
    Navigate to the `Your Data` view and try out the search and filter options.


### Volume Admin
This section is only relevant to Volume Admin users. 

Volume Admins will see an extra `Volume Data` option on the left side menu. From here you can view and perform actions on any data which has been registered in the DMA from a volume you are an admin of. 

To be granted volume admin permissions we need approval from the group leader who owns the volume. 

If you are a volume admin for more than one volume you can easily switch between the volumes using the dropdown menu at the top of the data list.

### User Preferences
#### Notifications
As previously mentioned, the requester will be notified by email when the status of an action changes. By default, email notifications are enabled for every action type, but this can be changed easily. 

Users can select which action types they wish to receive email alerts for. 

#### Hands On 11: Notification Settings
???+ abstract  "Hands On 11: Disable Notifications"
    1. Open the `Preferences` dialog by clicking on your user icon on the top right. 
    2. Disable email alerts for `Archive` and `Restore` actions.


### S3 Buckets
The DMA provides a way for users to provision their own S3 buckets, you simply need to provide a unique bucket name and budget number. Buckets are hosted on site using our MinIO service. 

The pricing will be the same as Tier 1 and is calculated based on actual usage, no need to pre-allocate any specific amount, you will be charged what you have used.

Buckets can also have a `quota` set which is the maximum size that bucket can be. You can set the quota at creation time, but you can also set and update it later via the DMA. 

You can find more information about S3 buckets on the [intranet](https://www.embl.org/internal-information/it-services/s3-object-store/). 

The DMA views buckets as another type of `data` and each bucket needs to be part of a collection. Buckets currently only support the `Share` action - allowing you to share a bucket with any other EMBL user. 

???+ note "Note"
    S3 bucket creation is not supported on the DMA Training Environment.


### Metadata
The DMA provides a flexible and customisable metadata model. Currently, metadata is only exposed via the API and is not visible via the UI, however if you have a use case for a certain type of metadata please reach out.

We are currently working with our colleagues in Hamburg to expose their Beamline metadata via the UI, allowing them to view, search and filter based on this metadata.

### Coming Soon
The DMA is still a work in progress, with many new features still in the pipelines. 

If there is a use case which has not already been covered and you think would be useful please feel free to reach out to us to discuss your needs. 

Here are a few features you should expect to see in the DMA soon.

#### Data Pull 
One of the goals when introducing the DMA was to allow you to track your data from the point of acquisition to archival. 

Data Pull will allow you to register your data directly at the point of acquisition, e.g. at a microscope machine.

You will then specify the location on the group share where the data should be transferred to and the DMA will take care of the data transfer completely transparently for you. 

#### Data Pullover
Similarly to Data Pull, Pullover will register data at the point of acquisition and specify where to transfer the data to, but will also include a handover to another user. 

One use case for this is to register your data on a microscope and to transfer the data to a LabID managed location on your group share. We are currently working with LabID to get this set up.

## Advanced DMA Usage for Developers
In this section we will dive into some advanced DMA features which may not be applicable to all users.

### DMA Scopes
The DMA supports the concept of Scopes to restrict access to some features. 

For example, volume admins are automatically elevated to the `VOLUME_ADMIN` scope when they log into the DMA UI, giving them access to more features than other users who access the DMA with the default `USER` scope. 

To change scope, you need to specify which scope to use when you log in. You will see this again when we cover logging in via the DMA API.

We also have several other scopes which are used to get access to certain actions such as handover. 

### Application Users
If you wish to integrate your own code or application with the DMA, you can do so by using an `Application User`. These users are tied to a scope, and any EMBL user who has permissions to use that scope will be able to use the DMA in the context of any application user tied to that scope.

For example, if you were developing an application (appX) with several members of your group which should integrate with the DMA to register data and archive the data, we would set up a special scope and application user on the DMA and give your team members permissions to use the new scope. Each team member would then be able to log in to the DMA in the context of the appX_user, but using their own EMBL credentials to do so. 

This means, if a team member leaves EMBL, the application user can still be used by other team members.

If you have a use case for this please reach out and we can help you get the user set up.

### DMA API
The DMA UI is built upon the DMA API, therefore everything you can do via the UI can also be done via the REST API. 

The API also includes some advanced features which are not yet exposed via the DMA UI such as partial data restore and metadata management. 

The DMA [API documentation](https://dma.embl.de/swagger-ui/index.html) defines every available endpoint, including example request body, expected response codes and example responses.

The API is also fully accessible on the DMA Training environment for testing purposes.

#### Authentication
To use the DMA API you will first need to generate a `Bearer Token` which you will then send along as part of any following request.

By default, tokens have a lifetime of 24 hours from the moment they are generated. We also offer the option to generate `permanent` tokens which have a lifespan of 10 years.

???+ abstract  "Hands On: API Login"
    Using the same user from the UI training above, we will now log into the DMA training env via the API.

    We will use basic curl commands to illustrate the API usage, but feel free to use tools such as Postman or Insomnia.

    Execute the following on the command line - for now you can ommit the `scope` and `application` parameters.

    === "cURL"
        ``` bash
        curl --request POST \
        --url https://dma-training.embl.de/api/v1/login \
        --header 'Content-Type: application/x-www-form-urlencoded' \
        --data username=<YOUR_USERNAME> \
        --data password=<YOUR_PASSWORD> \
        --data scope=<YOUR_SCOPE> [OPTIONAL] \
        --data application=<YOUR_APPLICATION_USER> [OPTIONAL]
        ```

    You can also store the generated DMA token in an environment variable. Execute the following the generate a token and store it in the `DMA_PW` variable which you can then use for all following calls.
    === "cURL"
        ``` bash
        export DMA_PW=`curl -s --request POST \
        --url https://dma-training.embl.de/api/v1/login \
        --header 'Content-Type: application/x-www-form-urlencoded' \
        --data username=<YOUR_USERNAME> \
        --data password=acciodata` 2>/dev/null
        ```

#### List data
The list data API endpoint allows you to fetch your data and also to apply search and filters. 

???+ abstract  "Hands On: Get Data"
    Execute the following on the command line, make sure add the token which you created in the previous step.

    === "cURL"
        ``` bash
        curl --request GET \
        --url 'https://dma-training.embl.de/api/v1/data' \
        --header "Authorization: Bearer $DMA_PW"
        ```

    You should now get a list of your data items. 

The GET `/data` endpoint also supports optional search and filter parameters including `nameContains`, `pathContains` etc.
You can also fetch data in a paginated way using the `offset` and `limit` options.

You can find a full list of the supported options [here](https://dma.embl.de/swagger-ui/index.html#/Data/getData).

???+ abstract  "Hands On: Search and Filter Data"
    Use the filters to get your `Herblogy` data which you registered in the UI section of the training.

    === "cURL"
    
        ``` bash
        curl --request GET \
        --url 'https://dma-training.embl.de/api/v1/data?nameContains=herbology' \
        --header "Authorization: Bearer $DMA_PW"
        ```

#### Create Collection & Register Data
To register data you will first need to have a valid collection ID to which you can assign the data. 

???+ abstract  "Hands On: Create New Collection"
    You can create a new collection by sending a POST request to the `/collections` endpoint.
    
    You can find more information about the endpoint [here](https://dma.embl.de/swagger-ui/index.html#/Collections/registerCollection).

    Let's create a new collection to add some data to.

    === "cURL"
        ``` bash
        curl --request POST \
        --url https://dma-training.embl.de/api/v1/collections \
        --header "Authorization: Bearer $DMA_PW" \
        --header 'Content-Type: application/json' \
        --data '{
            "name": "api_collection",
            "comment": "Test Collection Created via API"
        }'
        ```

    A JSON represenation of your collection will be returned. Take note of the `ID` value.
    

???+ abstract  "Hands On: Register Data"
    You can register data by sending a POST request to the `/data` endpoint. 
    
    You can find more information about the endpoint [here](https://dma.embl.de/swagger-ui/index.html#/Data/registerData).

    We will now register the `potions` directory which we deregistered earlier.

    Make sure to update the `path` value with your house and username and to use your bearer token and collection ID.

    === "cURL"
        ``` bash
        curl --request POST \
        --url https://dma-training.embl.de/api/v1/data/ \
        --header "Authorization: Bearer $DMA_PW" \
        --header 'Content-Type: application/json' \
        --data '{
            "collectionId":<YOUR_COLLECTION_ID>,
            "comment": "api data",
            "name":"potions",
            "path": "/tmp/hogwarts/<YOUR_HOUSE>/<YOUR_USERNAME>/potions"
        }'
        ```

    A JSON representation of your data will be returned. Take not of your **Data ID**, we will use this in the next steps to request actions.


#### Archive Data
To request an action for a data item you will need to know its data ID.

???+ abstract  "Hands On: Archive Data"
    We will now request archival of the potions data which you just registered. 
    === "cURL"
        ``` bash
        curl --request POST \
            --url https://dma-training.embl.de/api/v1/data/<YOUR_DATA_ID_HERE>/archive \
            --header "Authorization: Bearer $DMA_PW" \
            --header 'Content-Type: application/json' \
            --data '{
                "budgetNumber": 123455,
                "deleteData": "true",
                "comment": "Test Comment"
            }'
        ```

    Take note of the `ID` of the action which is returned.

    You can use this `ID` to get the status of the action:

    === "cURL"
        ``` bash
        curl --request GET \
            --url https://dma-training.embl.de/api/v1/actions/<ACTION_ID> \
            --header "Authorization: Bearer $DMA_PW" 
        ```

    Or you can simply fetch a list of all of your actions using the following endpoint, where you can also apply search and filter options to find actions in a given state or based on data ID, etc. 

    === "cURL"
        ``` bash
        curl --request GET \
            --url https://dma-training.embl.de/api/v1/actions \
            --header "Authorization: Bearer $DMA_PW" 
        ```

#### Restore Data
Restoring data via the API requires the following parameters:

- Data `id` of the data you want to restore an archive of.
- `archiveId` which is the ID of the exact archive of that data you wish to restore, because it is possible that there is more than 1 archived version of a data.
- `restoreTo` a path on your group share that has enough space for the restored data. If the directory does not exist, we will attempt to create it in your user context.

You can also specify a `limitTo` parameter if you wish to only restore a part of the archived data. This `limitTo` value must be the full path to a child directory or file from the archive you are restoring. The path must be exactly as it is found in the Archive Index for the archive you are restoring.

We will assume you already know the ID of the data you wish to restore a copy of and in the next hands on we will look at how we can restore data.

???+ abstract  "Hands On: Restore Data"
    The first thing we need to do is get the `archiveId` value of the data which we archived earlier. We can do this using the `/data/{id}/status` endpoint which will return an array of all existing archives for the given data, as well as information about other aspects such as shares, data availability on disk and more.

    === "cURL"
        ``` bash
        curl --request GET \
        --url https://dma-training.embl.de/api/v1/data/<DATA_ID_HERE>/status \
        --header "Authorization: Bearer $DMA_PW"
        ```

    Each archive object in the Archives array will contain an ID along with some other useful information such as archive size in bytes and the URL to download the Archive Index file. Please note that the size on the training environment will always be zero and the Index URL will not be valid. 

    Once you have the ID of the archive you wish to restore, and you have made sure you have enough space on your group share to restore it, you can then go ahead and submit the restore request. 

    === "cURL"
        ``` bash
        curl --request POST \
        --url https://dma-training.embl.de/api/v1/data/<YOUR_DATA_ID>/restore \
        --header "Authorization: Bearer $DMA_PW" \
        --header 'Content-Type: application/json' \
        --data '{
            "archiveId":<YOUR_ARCHIVE_ID>,
            "restoreTo":"/tmp/hogwarts/<YOUR_HOUSE>/<YOUR_USERNAME>/restored_data"
        }'
        ```

    The response will be an action object including the action ID which you can use to fetch the status of the action later. 

### Python Client
Code available on [EMBL Gitlab](https://git.embl.de/grp-itservices/dma-python-client).

Does not support all API functions - if something is missing feel free to open an issue, or even contribute.

???+ note  "Currently Supported"
    ```
        list_data
        get_data
        get_data_status
        list_actions
        register_data
        unregister_data
        register_collection
        unregister_collection
        request_archive
        request_delete
        request_share
        action_status
        request_restore
        request_handover
    ```

#### Installation
The python client can be installed using pip:

``` bash
pip install dma-client --extra-index-url https://git.embl.de/api/v4/projects/4393/packages/pypi/simple
```

You also have the option to add it to your `requirements.txt` file:

``` bash
--extra-index-url https://git.embl.de/api/v4/projects/4393/packages/pypi/simple
dma-client==0.0.21
```

#### Authentication
``` python
from dma_client import RESTClient

# NB: no hardcoded credentials!
username, password = 'ssnape', 'acciodata'

client = RESTClient('https://dma-training.embl.de/api/v1')
client.authenticate(username, password)

```


#### Listing Data
The Python client contains several models, one of which is the Data object. 

You can use the `list_data` method to fetch a dict containing all of your data from the DMA. 

```python
# Fetch a dict containing all of your DMA data items
all_data = client.list_data()
print(all_data)

# Apply filters while fetching data
filtered_data = client.list_data(
        name_contains='abc',
        path_contains='/g/group',
        comment_contains='test comment',
        deleted=False,
        data_type='DIRECTORY',
        additional_info=True,
)
print(data) 
```

#### Registering Data
The only mandatory parameters for data registration are data a valid path and collection ID. 
Once you have those you can register data as follows:

```python
# Create a new collection
new_collection = client.register_collection(name='Python Collection', comment="Created using python")
# Register some data in the newly created collection
new_data = client.register_data(path='/tmp/hogwarts/slytherin/ssnape/herbology', collection_id=new_collection.id)
print(new_data)
```

#### Requesting Actions
The python client currently supports requesting of the following actions:
- Archive
- Restore
- Share
- Delete
- Handover

Requesting an action will return an Action object, containing the action ID and more.

```python
archive_action = client.request_archive(
    data_id=13,
    budget_number=12345,
    delete_data=False,
    comment='test archive submitted via python client'
)
```

You can then fetch the status of this action using the returned ID. 

```python
action_id = archive_action.id
print(f'Requested Archive Action - ID {action_id}')

# Poll DMA for status change
while client.action_status(action_id) == "open":
    print(f'Action {action_id} is still pending')
    sleep(10)

status = client.action_status(action_id)
print(f'Action {action_id} - is {status}')
```

### Shell Client
You also have the option of interacting with the DMA using our Shell Client. 

You can access the code for the client on the [EMBL Gitlab](https://git.embl.de/grp-itservices/dma-shell-client).

The shell client is a Work In Progress - feel free to report any bugs or missing features via the Git Issues. 


#### Installation
The CLI tool requires that you have the `dma-client` already installed, once you have you can then install the shell client using pip:

``` bash
pip install dma-shell-client -U --extra-index-url https://git.embl.de/api/v4/projects/5019/packages/pypi/simple
```

Once installed you can access the client as follows:
```bash
dma-sc --help
```

You also have the option to use the Shell client in `shell` mode, which will provide you with useful hints as you type.
```bash
dma-sc shell
```

For the following examples we will use shell mode. 

#### Output
By default, the shell client will output data in a `table` format e.g.

```
┏━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━┳━━━━━━━━━┳━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━━━━━┳━━━━━━━━┓
┃ data_id ┃ path                                     ┃ name      ┃ owner  ┃ deleted ┃ comment ┃ created                   ┃ data_type ┃ collection_id ┃ status ┃
┡━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━╇━━━━━━━━╇━━━━━━━━━╇━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━╇━━━━━━━━━━━━━━━╇━━━━━━━━┩
│ 13      │ /tmp/hogwarts/slytherin/ssnape/potions   │ potions   │ ssnape │         │         │ 2023-10-23 13:14:15+02:00 │ directory │ 12            │        │
│ 12      │ /tmp/hogwarts/slytherin/ssnape/herbology │ herbology │ ssnape │         │         │ 2023-10-23 12:17:00+02:00 │ directory │ 12            │        │
└─────────┴──────────────────────────────────────────┴───────────┴────────┴─────────┴─────────┴───────────────────────────┴───────────┴───────────────┴────────┘
```

You also have the option to output `csv` and `text`. 

To change the output mode:
```bash
set-display-format text
```

You can also easily redirect output to a file:
```bash
dma-sc data ls > output.txt
```

#### Authentication
By default, the shell client will use the Production DMA instance, but you can easily override this. 

To log in to the Training Environment you can use the following command.

```bash
auth <YOUR_TRAINING_USER> --url https://dma-training.embl.de/api/v1
```

You will then be prompted to enter your password which is `acciodata`.

#### Listing Data
Once authenticated you can use the following command to list your data:

```bash
data ls   
```

You can also use the optional filter flags if you want to refine your result:
```bash
data ls --name-contains abc      
           --path-contains /tmp/test
           --comment-contains test
           --no-deleted   
           --data-type directory          
           --additional-info  
```


#### Registering Data

```bash
collection register "CLI Collection" "Created with the shell client"

data register /tmp/hogwarts/slytherin/ssnape/herbology --collection-id 14
```


#### Requesting Actions
The Shell Client currently supports the following actions:
- Archive
- Restore
- Share
- Delete
- Handover

To request the archive of data with ID 14:
```bash
action archive 14 12345 false "test comment"
```
 Where 14 is the Data ID, `12345` is your budget number, `false` indicated the data should not be deleted and a comment is included as the last parameter.

You can find more information on all available Shell Client commands on the README file in the [Git Repo](https://git.embl.de/grp-itservices/dma-shell-client). 






