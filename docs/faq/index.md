Here you will find answers to some commonly asked questions. If you still require some help do not hesistate to pop us an email ad dma@embl.de.

## General Issues
??? tip  "Why can I not see my group share when I try to register data?"

    Each group share needs to be enabled for use in the DMA. If you do not see a group share in the File Browser when registering data simply pop an email to dma@embl.de and we will enable the group share. Once enabled it will be visible for all group members in the DMA. 

## Sharing
??? tip  "Why is my share action failing?"

    To share data via the DMA, you need to make sure that you are the **OWNER** of the data on the file system. If you try and share a directory which contains even a single file which you do not own the share action will fail.

    Please make sure you have the correct file system level permissions on all data before you attempt to share the data.

    Sharing is not supported on "mirror" volumes, as these are read only and the DMA can not alter permissions there.

??? tip  "Why doesn't data moved into a DMA shared directory automatically inherit the shared permissions?"

    The issue arises from the nature of the `mv` command. When you move data using mv, it retains its original permissions, ignoring the permission settings of the DMA shared directory. This means the moved data does not automatically align with the shared directory's access rules.

    To maintain consistent access for all users, it's recommended to use the `cp` (copy) command. Copying data with `cp` creates a new version of the files or folders within the shared directory, allowing them to inherit the directory's permissions. This ensures that all shared data in the directory remains accessible to authorized users according to the established DMA sharing permissions.

    Using the `mv` command you may see error messages such as - `failed to preserve ownership for '/g/group/sharedDir/movedFile.txt': Permission denied` when you try move data into a DMA shared directory.

## Archive & Restore
??? tip  "How long will my Archive / Restore request take?"

    Archive and restoring data from tape is a slow process. It is not possible to give you a concrete answer on ho wlong it will tak, but you should expect it to take up to several days. 

    The amount of time depends on how large your data is and also on how busy the archive infrastruture is in the backend. The archive infrastruture is not only being used by the DMA, it is also used for the general backup of all group shares and by many other services, therefore the DMA has no control on how your request is prioritised once it is sent to the backend infrastructure. 

    The DMA does implement some fair queuing of archive and restore job requests to try and make sure users do not abuse the system. We are monitoring this and refining as we see fit.

??? tip  "I am archiving data from a "mirror" volume, and my Archive + Delete request did not delete the data."

    Mirror volumes are read-only and therefore the DMA can not delete the data from them. Once the archive request has finished, please take care to manually delete the data from the original location yourself.

## Delete
??? tip  "Why is my delete action failing?"

    To delete data via the DMA, you need to make sure that you have write permissions on all of the data, otherwise you will not be able to delete the data.

    Please make sure you have the correct file system level permissions on all data before you attempt to delete the data.

    If you are unable to alter file system permission of the data, one way around this is to request Archive + Delete of the data. The archive process will take care of the data deletion as the data is written to the tape archive. 

    Delete actions are not supported on "mirror" volumes as these are read-only. You should manually delete the data yourself and the mirror will be updated the next time the data is synced to Heidelberg.