# Restore

Data restoration involves retrieving data from the tape and placing it back into your group's shared storage.

When data has been archived, a list of accessible archives appears in the 'Archive & Restore' section within the Data overview panel. To restore an archive, simply click the "Restore" button next to the desired archive. You'll need to specify a destination path for the restored data. By default, this is set to the original parent directory of the data, but you can modify it as needed.

The data will continue to be stored on the tape archive even after restoration, allowing for future restorations if necessary.

Be aware that the restoration process is time-consuming. The duration depends on the data size and the current load on the archive infrastructure, potentially taking several days. You will be informed by email once your restore request has been completed.

Please ensure that the chosen restore location has **sufficient space** to accommodate the restored data. The required space is displayed in the DMA UI when initiating restoration. If adequate space is not available, the restoration will fail, potentially causing storage issues for your group.

## Partial Restore
You can choose to restore specific subdirectories or files from a larger archive. To do this, refer to the Archive Log File, which lists all files and folders within an archive. 

When opting for partial restoration, you must specify the `Limit To` path exactly as it is shown in the Archive Log File.

## Restore path limitations
The path which you specify to restore the Data to must be a valid IT provided group share. We do not support restoring data to other shares such as those provided by SCB.

Please be aware that it is not possible to restore data to a path that is on a "Mirror" volume, as these volumes are read-only and writing data to them is not possible. If you do wish to restore data that has been archived from a "Mirror" volume, please choose a path on a different volume when selecting a path to restore to. This applies mainly to users in Rome and Barcelona.

If the path provided points to a directory which does not exist, the DMA will attempt to create the directory on the requesting user's behalf. If this fails due to permissions the user will be notified.