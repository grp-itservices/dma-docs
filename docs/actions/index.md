# DMA Actions

An action is a request to perform some sort of file system operation on a collection or data item. Actions can take some time to complete, and you will be informed via email when an action status changes.

Actions have 3 states - open, done, and failed.

You will be informed by email when the status of an action changes (if email notifications are enabled, which they are by default).

Each Data/Collection can only have 1 pending action at a time, therefore you cannot request an action if there is already 1 action for that item in an open state.

You can see the history of all requested actions for data in the 'Actions' tab of the data overview panel.

From here, you can also request new archive, restore, and share actions.