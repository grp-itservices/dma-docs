# Share

## Group Volume Data
The DMA provides a way for you to share your group volume based data with other EMBL members by adjusting the file system permissions, thereby granting them access.

You can share your DMA registered data with any member of EMBL. Within the 'Share' section, simply input the EMBL username of the individual and define their access level (Read Only / Read & Write). There's also an option to include a comment, which will be conveyed to the user.

Upon successful data sharing, both you and the recipient will be notified via email about the completion of the sharing process.

Please be aware that **you need to be the owner** of the data you are sharing. If any part of the data is owned by someone else in the file system, the share request will fail. 

Additionally, it is not possible to share data that is on a "Mirror" volume, as these volumes are read-only and modifying the file system permissions on them is not possible. This applies mainly to users in Rome and Barcelona.

### Types of sharing
#### Read Only
The selected user will get read only access to the data. If the data item is a directory they will get read only access to all files and directories inside the registered directory.

#### Read-Write 
The selected user will be able to be read and write the data. If the data is a directory they will be able to read & write all the content of the directory, including all files and directories. 

???+ warning  "Important Note for Read + Write Sharing"
     Granting read+write access to a directory may permanently prevent resharing via the DMA. Collaborators who create new files or subdirectories within the shared directory will own those items. Since the DMA requires full ownership of all directory contents to share it, you will lose resharing capability if collaborators add their own files.

### Permission Inheritance for Data Transferred into a DMA Shared Directory
When sharing a directory via the DMA with Read + Write access, users can add new data. To ensure proper permission inheritance, data should be either created directly within the shared directory or copied into it using the `cp` command by the users who the directory is shared with. 

This is crucial because copying, i.e. using the `cp` command, creates a new instance of the data, inheriting the directory's permissions. In contrast, moving the data with the `mv` command can lead to permission inconsistencies. This occurs because `mv` transfers both the data and its original permissions, bypassing the shared directory's permission which were applied via the DMA.

## Buckets
S3 Buckets which are created via the DMA can be shared with any EMBL user. Just like file system based sharing, you can specify the level of permissions which the user should have (Read Only / Read & Write). Once the bucket is shared, the user will be informed by email and will be able to access the data via the EMBL Minio UI. 

## Shared Data View
The `Shared Data` view on the left hand menu allows you to quickly see all data which other users have shared with you via the DMA.