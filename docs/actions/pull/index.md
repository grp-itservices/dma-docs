# Pull

Currently this action is only available via the DMA API. 

Data pull allows you to fetch data from a remote machine (e.g. a microscope) and copy the data to your group share. 

This action requires special access privileges and also requires that the remote machine is set up and configured to be accessible by the DMA to perform the pulling of data. If you believe you have a use case for this action please let us know and we can set up access for you.

## Pullover
Data pull can also be combined with a Handover action. This allows you to pull data from a remote machine and give the pulled data to another user. 

## Technical Details

### Trailing Slash in the Data Source
When specifying the source of the data you should be aware that the action behaves differently depending on the presence of a trailing slash in the given source path.

If a trailing slash is provided, then the content of the specified directory will be copied to the specified target directly. 

If no trailing slash is provided, then the directory itself, including all content, will be copied to the specified target location. 

The following table gives a few examples of this:

| Source | Target | Registered in DMA |  | 
| --- | --- | --- | --- | 
| /c/local/directory1 | /g/its/target1 | /g/its/target1/directory1 | Directory `directory1` is copied into the `target1` directory and the copied `directory1` folder is registered in the DMA |
| /c/local/directory1/ | /g/its/target2 | /g/its/target2 | The contents of `directory1` are copied into the `target2` directory and the `target2` folder is registered in the DMA |
| /c/local/directory1/file1.txt | /g/its/target3 | /g/its/target3/file1.txt | The file `file1.txt` is copied into the target directory and the file is registered in the DMA |
