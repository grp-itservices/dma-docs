# Archive

Archival involves moving the data to tape; it can be restored from tape at a later date.

Data which is archived via the DMA will be written to tape for a period of **10 years**. If you restore the data during this time, the original copy will remain on tape until the 10-year period has passed.

You can find more information about the Archival process [here](https://www.embl.org/internal-information/it-services/central-data-storage-file-systems-heidelberg/) - under the "Large Volume Archiving" heading.

Archive requests take time, how much time depends on several factors including the size of your data and how busy the archive infrastructure is. It is normal for archival to take several days to complete. You will then be informed by email regarding the status of the archival process. 

After submitting an archive request, you **should not modify your data** until the archive request has been marked as complete. If you do edit your data while there is an Archive request open, the version of your data which is written to tape might not be the version you expect. Editing data during the archive process may also lead to the archive request failing. 

## Costs
You will be asked to provide the budget number which will be charged for the archival. You can find the current fees at the end of [this page](https://www.embl.org/internal-information/it-services/central-data-storage-file-systems-heidelberg/).

Please make sure you are authorised to request the archive using the budget number you specify.

## Archive + Delete
You can also specify that the data should be deleted from its current location on disk once the archival has been completed. All files will be deleted once they have been transferred to tape. 

It is important to note, this deletion ignores file permissions, and therefore allows you to delete data which you may not have permission to delete. As the data has already been written to tape you can always restore the data later, however we still encourage you to make sure you are authorised to delete the data before you submit an archive + delete request. 

This option will only delete files, the directory structure will be left in place. You can go ahead and clean up this directory structure manually if you wish once the archive is complete.

Please be aware that the Archive + Delete option is not available for data which is archived using one of the "Mirror" volumes. These volumes are read-only and therefore deletion from the is not possible. If you wish to Archive + Delete data from one of these mirrors, please request an Archive (without delete) and once the archive process has been finished you should then delete the data from the original location manually. This applies mainly to users in Rome and Barcelona.

## Archival of large directories
Should you frequently need to archive directories containing an exceptionally high number of files (for instance, 100,000 or more), we recommend that you initially compress these directories into a tar file before archiving. For any queries or assistance regarding this process, please feel free to reach out to us.

## Archive Log File
Once an archive process is complete a full list of all files which were archived as part of the request will be created. You can download this index from the DMA UI, just browse to the Archive panel, where you will see a list of all existing Archives for the selected data item. Each archive will have `Download Archive Log File` button which will trigger the file download.

The format of this file is subject to change, so we would discourage any automated processing of these files.

## Collection Archive
You also have the option to request archival on the collection level. Doing this will simply open an archive request for each data item in the collection. Once all of these archive requests have been processed you will be informed by email.

