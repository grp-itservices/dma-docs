# Handover

Currently this action is only available via the DMA API. 

Handover allows you to give data to another user. It involves copying the data to a specified location and setting the user of the data to the new owner. 

This action is only available to users with an elevated scope. If you believe you have a use case for this action please let us know and we can set up access for you.


