## Archive Downtime for Rome Based Data
04.12.2024 - 06.12.2024

The mirroring of data between Rome and Heidelberg will be paused from December 4-6 due to essential upgrades to the volume infrastructure in Rome.

Please do not submit archive requests for Rome-based data during this period, as it may result in incomplete or incorrect archiving. Normal archiving operations will resume on Friday, December 6.

For any questions or concerns, please contact us at dma@embl.de.

Thank you for your understanding.





