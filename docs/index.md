---
hide:
  - navigation
  - toc
---


# Data Management Application Docs
The **Data Management Application (DMA)** is a flexible and customizable tool to assist you as an EMBL researcher in managing your data and documenting your data lifecycle from production to archiving. The DMA supports you to keep track of your data’s location, ownership as well as metadata and to perform various data operations on the IT infrastructure without changing any of your existing workflows.

The application is available at [https://dma.embl.de](https://dma.embl.de).

There is also a test system available at [https://dma.test.embl.de](https://dma.test.embl.de) where you can find the latest features.

An API is also available, further information can be found [here](https://dma.embl.de/swagger-ui/index.html).
