site_name: DMA Docs
site_author: IT Services
site_description: >-
  Documentation for the Data Management Application, developed by EMBL IT Services.
copyright: Copyright &copy; 2023 IT Services

nav:
  - Home: 'index.md'
  - Getting Started:
      - DMA Intro: intro/index.md
      - Quick-start: 'quick_start/index.md'
  - Actions:
      - Share: 'actions/share/index.md'
      - Archive: 'actions/archive/index.md'
      - Restore: 'actions/restore/index.md'
      - Delete: 'actions/delete/index.md'
      - Handover: 'actions/handover/index.md'
      - Pull: 'actions/pull/index.md'
  - Advanced:
      - S3 Buckets: 'advanced/s3-buckets/index.md'
      - Volume Admin: 'advanced/volume-admin/index.md'
      - Application Users & Scopes: 'advanced/application_users/index.md'
      - API: 'advanced/api/index.md'
      - Python Client: 'advanced/python-client/index.md'
      - CLI: 'advanced/cli/index.md'
      - Release Notes: 'release-notes/index.md'
  - Training: 'training/index.md'
  - FAQ: 'faq/index.md'
  - Announcements: 'announcements/index.md'

theme:
  name: material
  palette:
    primary: green
  features:
    - navigation.instant
    - navigation.tabs
    - navigation.top
    - toc.integrate
    - content.code.copy
  icon:
    admonition:
      abstract: fontawesome/solid/hand

markdown_extensions:
  - admonition
  - pymdownx.details
  - pymdownx.superfences
  - pymdownx.highlight:
      anchor_linenums: true
      line_spans: __span
      pygments_lang_class: true
  - pymdownx.inlinehilite
  - pymdownx.snippets
  - pymdownx.superfences
  - pymdownx.tabbed:
      alternate_style: true